require 'slack-ruby-bot'

class WeeklyTrainingLunchBot < SlackRubyBot::Bot
  help do
    title 'Lunch Bot'
    desc 'This bot allows you to schedule new WTL orders.'

    command 'choose' do
      desc 'Choose a restaurant for the next WTL.'
      long_desc 'Choose a restaurant for the next event. ' \
                'Example: `choose Subway`.'
    end

    command 'new wtl' do
      desc 'Creates a WTL event and provides three restaurant options.'
      long_desc 'Creates a WTL event for the next Monday and provides three ' \
                'restaurant options that have not been used recently. ' \
                'Restaurants with the same type of food as last week will ' \
                'also be excluded.'
    end

    command 'random' do
      desc 'Provides three random restaurants.'
      long_desc 'Provides three restaurant options that have not been used ' \
                'recently. Restaurants with the same type of food as last ' \
                'week will also be excluded.'
    end

    command 'tiebreaker' do
      desc 'Breaks ties when restaurants receive the same number of votes.'
      long_desc 'Provide a list of restaurants separated by "vs" and Lunch ' \
                'Bot will break the tie by the highest average user rating. ' \
                'Example: `tiebreaker Toast vs Ninth Street vs Luna`.'
    end
  end

  command 'hello' do |client, data, _match|
    client.say(channel: data.channel, text: 'Hello')
    sleep 1
    client.say(channel: data.channel, text: "It's me")
    sleep 1
    msg = "I was wondering if after all these years you'd like to eat :chompy:"
    client.say(channel: data.channel, text: msg)
  end

  command 'choose' do |client, data, match|
    name = match['expression'].to_s.strip.gsub('’', "'")
    # Convert any HTML entities such as &amp;
    name = Nokogiri::HTML.parse(name).text
    restaurant = Restaurant.find_by(name: name)
    event = Event.next

    if restaurant.blank?
      message = "No restaurant with name #{name} found."
    elsif event
      event.update!(restaurant: restaurant)
      message = "*#{restaurant}* has been chosen for the event: " \
                "#{event} on #{event.occurs_on}. Please place your orders.\n" \
                "Menu: #{restaurant.menu_url}\nOrder doc: #{ENV['ORDER_DOC']}"
    else
      message = 'No upcoming events found'
    end

    client.say(channel: data.channel, text: message)
  end

  command 'new wtl' do |client, data, _match|
    event_attrs = {
      name: 'Weekly Training Lunch',
      occurs_on: Time.zone.today.next_occurring(:monday)
    }

    event = Event.find_by(event_attrs)

    if event
      message = "Next week's training lunch has already been created. " \
                "#{event.restaurant || 'No restaurant'} has been selected."
    else
      Event.create!(event_attrs)

      message = "Please vote for the restaurant for next week's WTL.\n"
      message += random_options(event.occurs_on)
    end

    client.say(channel: data.channel, text: message)
  end

  command 'random' do |client, data, _match|
    client.say(channel: data.channel, text: random_options)
  end

  command 'tiebreaker' do |client, data, match|
    restaurant_names = match['expression'].to_s.strip.gsub('’', "'")

    message = Tiebreaker.new(restaurant_names).tiebreaker_message
    client.say(channel: data.channel, text: message)
  end

  def self.random_options(time = Time.zone.now)
    emoji = [':popcorn:', ':cookie:', ':apple:', ':pancakes:']

    random_restaurants(time).map.with_index do |restaurant, index|
      "#{emoji[index]} #{restaurant.name} #{restaurant.menu_url}"
    end.join("\n")
  end

  def self.random_restaurants(time)
    weighter = OptionWeighter.new(selectable_restaurants(time))
    weighted_options = weighter.weight_options do |restaurant|
      UserRating.where(restaurant: restaurant).sum(:rating)**2
    end

    final_options = []
    final_options << weighted_options.sample until final_options.uniq.size == 4

    final_options.uniq
  end

  # Only allow restaurants that:
  # 1.) Have not been used in the last month
  # 2.) Do not have the same type of food as restaurants chosen in the past week
  # 3.) Are not incompatible with any users' diets
  def self.selectable_restaurants(time)
    restaurants_without_recent_picks(time).reject do |restaurant|
      restaurant.diet_incompatibilities.any?
    end
  end

  def self.restaurants_without_recent_picks(time)
    restaurants = Restaurant.includes(:diet_incompatibilities)
    recent_picks = Restaurant.recent_picks(time)
    restaurants -= recent_picks
    last_food_type = recent_picks.first.food_type

    if last_food_type.present?
      restaurants.reject! do |restaurant|
        restaurant.food_type.to_s.downcase == last_food_type.downcase
      end
    end

    restaurants
  end
end
