class CreateDietIncompatibilities < ActiveRecord::Migration[5.2]
  def change
    create_table :diet_incompatibilities do |t|
      t.references :restaurant, null: false
      t.references :user, null: false
      t.timestamps
    end
  end
end
