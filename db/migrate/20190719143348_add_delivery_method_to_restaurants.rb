class AddDeliveryMethodToRestaurants < ActiveRecord::Migration[5.2]
  def change
    add_column :restaurants, :delivery_method, :string
    Restaurant.find_each do |restaurant|
      url = restaurant.menu_url

      if url.include?('grubhub')
        delivery_method = 'GrubHub'
      elsif url.include?('takeoutcentral')
        delivery_method = 'Takeout Central'
      elsif url.include?('doordash')
        delivery_method = 'DoorDash'
      else
        delivery_method = 'Provided by restaurant'
      end

      restaurant.update!(delivery_method: delivery_method)
    end
  end
end
