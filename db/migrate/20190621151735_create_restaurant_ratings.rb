class CreateRestaurantRatings < ActiveRecord::Migration[5.2]
  def change
    create_table :restaurant_ratings do |t|
      t.integer :stars, null: false, default: 3
      t.references :restaurant, index: true
      t.references :user, index: true
      t.timestamps
    end

    add_index :restaurant_ratings, %i[restaurant_id user_id], unique: true
  end
end
