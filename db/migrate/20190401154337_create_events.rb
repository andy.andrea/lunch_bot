class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :name, null: false
      t.date :occurs_on, null: false, index: true
      t.references :restaurant, index: true
      t.timestamps
    end
  end
end
