class User < ApplicationRecord
  has_paper_trail
  devise :omniauthable, :timeoutable, :rememberable,
         omniauth_providers: [:google_oauth2]

  validates :email, presence: true, uniqueness: true

  has_many :restaurant_ratings, dependent: :destroy
  has_many :diet_incompatibilities, dependent: :destroy
  # View; no dependent option needed
  has_many :user_ratings
end
