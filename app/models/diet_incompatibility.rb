class DietIncompatibility < ApplicationRecord
  validates :restaurant_id, uniqueness: { scope: :user_id }

  belongs_to :restaurant
  belongs_to :user
end
