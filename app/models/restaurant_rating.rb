# A restaurant rating is a table-backed model that stores any rating information
# that has been added manually by a user. Currently, ratings are on a scale from
# 1 to 5 stars. This model is different from UserRating which is backed by a
# view and has a row for every user/restaurant combination (even if the user has
# not yet rated that restaurant).
class RestaurantRating < ApplicationRecord
  validates :stars, presence: true
  validates :stars, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 1,
    less_than_or_equal_to: 5
  }
  validates :restaurant_id, uniqueness: { scope: :user_id }

  belongs_to :restaurant
  belongs_to :user
end
