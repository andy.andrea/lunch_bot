class Restaurant < ApplicationRecord
  has_paper_trail

  validates :food_type, presence: true
  validates :delivery_method, presence: true
  validates :menu_url, presence: true
  validates :menu_url, uri: true
  validates :name, presence: true, uniqueness: { case_sensitive: false }

  has_many :events, dependent: :destroy
  has_many :diet_incompatibilities, dependent: :destroy
  has_many :restaurant_ratings, dependent: :destroy
  # View; no dependent option needed
  has_many :user_ratings

  scope :recent_picks, ->(time = Time.zone.today) do
    joins(:events)
      .order(occurs_on: :desc)
      .where(
        Event.arel_table[:occurs_on].lt(time).and(
          Event.arel_table[:occurs_on].gt(time - 1.month)
        )
      )
  end

  def to_s
    name
  end

  def rating_for(user)
    restaurant_ratings.find { |rating| rating.user_id == user.id }
  end

  def average_rating
    user_ratings.average(:rating)
  end
end
