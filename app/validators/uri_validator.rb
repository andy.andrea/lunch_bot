class UriValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return true if value.blank?

    URI.parse(value).is_a?(URI::HTTP)
  rescue URI::InvalidURIError
    record.errors[attribute] << (options[:message] || 'is an invalid URL')
    false
  end
end
