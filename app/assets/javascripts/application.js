//= require jquery
//= require rails-ujs
//= require semantic-ui
//= require_tree .

$(document).ready(
  function() {
    $('.dropdown:not([data-allow-additions])').dropdown({
      placeholder: false,
      ignoreDiacritics: true,
      fullTextSearch: 'exact'
    });

    $('.dropdown[data-allow-additions]').dropdown({
      allowAdditions: true,
      ignoreDiacritics: true,
      fullTextSearch: 'exact'
    });

    $('.ui.date.calendar').calendar({
      type: 'date'
    });

    $('.ui.rating').rating({
      icon: 'ice cream',
      maxRating: 5,
      onRate: function(rating) {
        $.ajax({
          method: 'POST',
          url: $(event.currentTarget).data('url'),
          data: { stars: rating }
        }).done((_response) => {
          $('body').toast({
            message: 'Rating saved',
            class: 'success',
            position: 'top center',
            displayTime: 1000,
            showProgress: 'bottom'
          });
        }).fail((_response) => {
          $('body').toast({
            message: 'Something went wrong',
            class: 'error',
            position: 'top center',
            displayTime: 1000,
            showProgress: 'bottom'
          });
        });
      }
    });
  }
);
