class DietIncompatibilitiesController < ApplicationController
  before_action :set_diet_incompatibility, only: :destroy

  def index
    @diet_incompatibilities = DietIncompatibility
      .where(user: current_user)
      .page(params[:page])
  end

  def new
    @diet_incompatibility = DietIncompatibility.new
  end

  def create
    @diet_incompatibility = current_user.diet_incompatibilities.build(
      diet_incompatibility_params
    )

    if @diet_incompatibility.save
      flash[:success] = 'Incompatibility registered.'
      redirect_to diet_incompatibilities_path
    else
      render :new
    end
  end

  def destroy
    if @diet_incompatibility.destroy
      flash[:success] = 'Incompatibility was successfully destroyed.'
    else
      flash[:warning] = @diet_incompatibility.errors.to_sentence
    end

    redirect_to diet_incompatibilities_path
  end

  private def set_diet_incompatibility
    @diet_incompatibility = DietIncompatibility.find(params[:id])
  end

  private def diet_incompatibility_params
    params.require(:diet_incompatibility).permit(:restaurant_id)
  end
end
