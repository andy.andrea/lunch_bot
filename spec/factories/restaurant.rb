FactoryBot.define do
  factory :restaurant do
    food_type { 'tasty' }
    menu_url { 'https://www.google.com' }
    sequence(:name) { |n| "Restaurant #{n}"}
  end
end
