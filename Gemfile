# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.3'

gem 'pg'
gem 'puma'
gem 'rails', '~> 5.2.2'
gem 'sass-rails'
gem 'uglifier'

gem 'async-websocket', '~> 0.8.0'
gem 'bootsnap', require: false
gem 'devise'
gem 'fomantic-ui-sass'
gem 'jquery-rails'
gem 'kaminari'
gem 'omniauth-google-oauth2'
gem 'paper_trail'
gem 'ransack'
gem 'scenic'
gem 'slack-ruby-bot'

group :development, :test do
  gem 'database_cleaner'
  gem 'pry-rails'
  gem 'rspec-rails'
end

group :development do
  gem 'listen'
  gem 'rubocop'
  gem 'rubocop-rspec'
  gem 'spring'
  gem 'spring-watcher-listen'
  gem 'web-console'
end

group :test do
  gem 'capybara'
  gem 'chromedriver-helper'
  gem 'factory_bot_rails'
  gem 'selenium-webdriver'
  gem 'shoulda-matchers'
  gem 'simplecov'
end
