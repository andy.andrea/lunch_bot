# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for(
    :users,
    skip: [:registrations],
    controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }
  )
  root 'restaurants#index'

  devise_scope :user do
    get 'signin' => 'devise/sessions#new', as: :new_user_session
    post 'signin' => 'devise/sessions#new', as: :session
    delete 'signout' => 'devise/sessions#destroy', as: :destroy_user_session
  end

  resources :diet_incompatibilities, only: %i[index new create destroy]

  resources :restaurants do
    collection do
      post :search
      get :search, to: redirect('/restaurants')
    end
  end

  resources :events do
    collection do
      post :search
      get :search, to: redirect('/events')
    end
  end

  resources :restaurant_ratings, only: :create
end
